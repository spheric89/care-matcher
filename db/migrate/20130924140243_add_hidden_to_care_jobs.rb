class AddHiddenToCareJobs < ActiveRecord::Migration
  def change
    add_column :care_jobs, :hidden, :boolean, default: false
  end
end
