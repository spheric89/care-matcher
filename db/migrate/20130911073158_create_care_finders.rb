class CreateCareFinders < ActiveRecord::Migration
  def change
    create_table :care_finders do |t|
      t.string :first_name
      t.string :last_name
      t.string :postcode

      t.timestamps
    end
  end
end
