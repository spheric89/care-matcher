class CreatePrefLocation < ActiveRecord::Migration
  def change
    create_table :pref_locations do |t|
      t.integer :postcode
      t.string :suburb
      t.string :town
    end
  end
end
