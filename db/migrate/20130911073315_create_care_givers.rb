class CreateCareGivers < ActiveRecord::Migration
  def change
    create_table :care_givers do |t|
      t.string :first_name
      t.string :last_name
      t.string :hourly_rate
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
