class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :from
      t.string :to
      t.text :message
      t.string :job

      t.timestamps
    end
  end
end
