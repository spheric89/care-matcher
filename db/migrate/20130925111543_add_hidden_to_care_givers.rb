class AddHiddenToCareGivers < ActiveRecord::Migration
  def change
    add_column :care_givers, :hidden, :boolean, default: false
  end
end
