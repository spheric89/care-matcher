class RemoveJobFromContacts < ActiveRecord::Migration
  def change
    remove_column :contacts, :job
  end
end
