class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :name
      t.decimal :price
      t.string :stripe_customer_token
      t.timestamps
    end
  end
end
