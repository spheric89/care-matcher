class AddCareFinderIdToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :care_finder_id, :integer
  end
end
