class AddExtrasToCareJob < ActiveRecord::Migration
  def change
    add_column :care_jobs, :smoker, :boolean
    add_column :care_jobs, :animals, :boolean
  end
end
