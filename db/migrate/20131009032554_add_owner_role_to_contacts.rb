class AddOwnerRoleToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :owner_role, :string
  end
end
