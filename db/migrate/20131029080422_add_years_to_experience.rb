class AddYearsToExperience < ActiveRecord::Migration
  def change
    add_column :experiences, :years, :integer
  end
end
