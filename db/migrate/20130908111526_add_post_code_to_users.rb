class AddPostCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :postcode, :integer
  end
end
