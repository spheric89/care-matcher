class CreateCareJobs < ActiveRecord::Migration
  def change
    create_table :care_jobs do |t|
      t.string :name
      t.text :description
      t.string :rate

      t.timestamps
    end
  end
end
