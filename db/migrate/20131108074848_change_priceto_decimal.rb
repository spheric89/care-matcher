class ChangePricetoDecimal < ActiveRecord::Migration
  def change
    remove_column :subscriptions, :price, :decimal
    add_column :subscriptions, :price, :integer
  end
end
