class CareNeeds < ActiveRecord::Migration
  def change
    create_table :care_needs do |t|
      t.string :name
      t.timestamps
    end
  end
end
