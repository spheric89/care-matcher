class AddToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :care_giver_id, :integer
    add_column :contacts, :care_job_id, :integer
  end
end
