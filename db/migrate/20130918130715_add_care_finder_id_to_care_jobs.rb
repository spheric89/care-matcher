class AddCareFinderIdToCareJobs < ActiveRecord::Migration
  def change
    add_column :care_jobs, :care_finder_id, :integer
  end
end
