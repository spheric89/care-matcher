class AddAttachmentPhotoToCareGivers < ActiveRecord::Migration
  def self.up
    add_column :care_givers, :photo_file_name, :string
    add_column :care_givers, :photo_content_type, :string
    add_column :care_givers, :photo_file_size, :integer
    add_column :care_givers, :photo_updated_at, :datetime
  end

  def self.down
    remove_column :care_givers, :photo_file_name
    remove_column :care_givers, :photo_content_type
    remove_column :care_givers, :photo_file_size
    remove_column :care_givers, :photo_updated_at
  end
end
