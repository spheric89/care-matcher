class AddColumnsToCareGivers < ActiveRecord::Migration
  def change
    add_column :care_givers, :smoker, :boolean
    add_column :care_givers, :animals, :boolean
  end
end
