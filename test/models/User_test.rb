require 'test_helper'

describe User do
  let(:user){build(:user)}

  it "is valid with correct params" do
    user.must_be :valid?
  end

  it "is invalid without a matching password" do
    build(:user, password_confirmation: 'mismatch').must_be :invalid?
  end

  it "is invalid without a password" do
    build(:user, password: nil, password_confirmation: nil).must_be :invalid?
  end

  it "is invalid without an email" do
    build(:user, email: nil).must_be :invalid?
  end

  it "is invalid without a postcode" do
    build(:user, postcode: nil).must_be :invalid?
  end 

  it "must have a unique password" do
    create(:user, email: 'test@test.com')
    build(:user, email: 'test@test.com').must_be :invalid?
  end

  it "is invalid without a role" do 
    build(:user, role_type: nil).must_be :invalid?
  end

  it "is invalid without the role CareGiver or CareFinder" do
    build(:user, role_type: 'asdf').must_be :invalid?
    build(:user, role_type: 'CareGiver').must_be :valid?
    build(:user, role_type: 'CareFinder').must_be :valid?
  end
end
