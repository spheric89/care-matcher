require "test_helper"

describe CareFinder do
  before do
    @care_finder = CareFinder.new
  end

  it "must be valid" do
    @care_finder.valid?.must_equal true
  end
end
