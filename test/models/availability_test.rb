require "test_helper"

describe Availability do
  before do
    @availability = Availability.new
  end

  it "must be valid" do
    @availability.valid?.must_equal true
  end
end
