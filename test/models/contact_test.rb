require "test_helper"

describe Contact do
  before do
    @contact = Contact.new
    @contact.care_job_id = 1
  end

  it "must be valid" do
    @contact.valid?.must_equal true
  end
end
