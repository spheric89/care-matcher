require "test_helper"

describe UserMailer do
  it "signup_confirmation" do
    user = build(:user, email: "test@test.com")
    mail = UserMailer.signup_confirmation(user)
    mail.subject.must_equal "Signup Confirmation"
    mail.to.must_equal ["test@test.com"]
    mail.from.must_equal ["from@example.com"]
    mail.body.encoded.must_match "THANKS FOR SIGNING UP"
  end
end
