FactoryGirl.define do
  factory :user do
    email {Faker::Internet::email}
    password 'test1234'
    password_confirmation 'test1234'
    postcode 4171
    role { |a| a.association(:care_giver)}
    association(:subscription)
    end

end







