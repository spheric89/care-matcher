# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :qualification, :class => 'Qualifications' do
    name "MyString"
  end
end
