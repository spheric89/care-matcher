# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :care_job do
    name "MyString"
    description "MyString"
    rate "MyString"
  end
end
