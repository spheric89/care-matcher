# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :care_finder do
    first_name "MyString"
    last_name "MyString"
    postcode "MyString"
  end
end
