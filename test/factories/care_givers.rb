# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :care_giver do
    first_name {Faker::Name::first_name}
    last_name {Faker::Name::last_name}
    hourly_rate {rand(1000)}
    description "MyString"
    image "MyString"
  end
end


