# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact do
    from "MyString"
    to "MyString"
    message "MyText"
    job "MyString"
  end
end
