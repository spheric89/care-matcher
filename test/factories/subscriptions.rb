# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :subscription do
    name 'Looking for Care Plan'
    paid 'False'
    price 50
  end
end
