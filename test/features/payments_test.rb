require "test_helper"

feature "Payments" do
  given(:cc){ OpenStruct.new(number: 4242424242424242, expires: 1220, cvc: 200) }
  scenario "Unpaid user accessing contact page" do
    login
    visit conversations_path
    page.must_have_content "Please subscribe to view"
    page.wont_have_content "Conversations"
  end
  
  scenario "CF subscribes" do
    login
    visit login_path
    click_link "My Account"
    click_link "Manage Subscription"
    page.must_have_content "Upgrade your membership by subscribing below."
    page.must_have_content "Looking for Care Plan"
    click_button "stripe-button-el"
    page.must_have_content ".overlay active"
  end

  scenario "CG subscribes" do
    login
    visit login_path
    click_link "My Account"
    click_link "Manage Subscription"
    page.must_have_content "Upgrade your membership by subscribing below." 
    page.must_have_content "Providing Care Plan"
    click_button "Subscribe"
    page.must_have_content ".overlay active"
    enter_credit_card
  end

  scenario "Unpaid user accessing conversation page" do

    

  end

private 

  def enter_credit_card
    fill_in 'card_number', with: cc.number  
    fill_in 'cc-exp', with: cc.expires
    fill_in 'cc-csc', with: cc.cvc
  end

end
