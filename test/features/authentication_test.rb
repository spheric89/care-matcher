require "test_helper"

feature "Authentication" do
  given(:user){ build(:user) }

  scenario "CareGiver can sign up" do
    visit '/'
    page.must_have_content "Sign Up"
    within "#new_user" do
      fill_in_form
      choose 'user_role_caregiver'
    end
      click_button "Sign Up"
      page.must_have_content "Thank you for signing up as a CareGiver!"
  end
  
  scenario "CareFinder can sign up" do
    visit '/'
    page.must_have_content "Sign Up"
    within "#new_user" do
      fill_in_form
      choose 'user_role_carefinder'
    end
    click_button "Sign Up"
    page.must_have_content "Thank you for signing up as a CareFinder!"
  end

  scenario "User can log in" do
    login
    visit login_path
    page.must_have_content "Logged in as test@test.com"
  end
  
  scenario "User can log out" do 
    login
    visit login_path
    page.must_have_content "Log Out"
    click_link "Log Out"
    page.must_have_content "Logged Out!"
  end

private

  def fill_in_form 
    fill_in 'Email', with: user.email
    fill_in 'user_password', with: user.password
    fill_in 'Password confirmation', with: user.password_confirmation
    fill_in 'Postcode', with: user.postcode
  end
end
