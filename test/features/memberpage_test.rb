require "test_helper"

feature "Memberpage" do
  given(:caregiver){build(:user)}

  scenario "access page as caregiver" do
    create(:user)
    login
    page.must_have_content "Manage Profile"
  end

end
