ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"
require "active_record/railtie"
require "action_controller/railtie"
require 'rake/testtask'
require "action_mailer/railtie"
require "sprockets/railtie"
require "minitest/rails/railtie"
require "minitest/rails/capybara"
require 'rack_session_access/capybara'

# Uncomment for awesome colorful output
require "minitest/pride"

Rails.application.config do
    config.middleware.use RackSessionAccess::Middleware
end

class ActiveSupport::TestCase
  
  include FactoryGirl::Syntax::Methods

  before :each do 
    DatabaseCleaner.clean
  end

  def login
    create(:user, email: 'test@test.com')
    visit login_path
    page.must_have_content "Log in"
    within "#login_form" do
      fill_in 'Email', with: 'test@test.com'
      fill_in 'Password', with: 'test1234'
    end
    click_button "Log in"
  end
  
  def setup
    
  end

  def teardown

  end

 
end
