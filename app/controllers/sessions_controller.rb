class SessionsController < ApplicationController

  before_filter :authorize, only: :destroy

  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to '/'
    else
      flash[:alert] = "Email or Password is invalid."
      redirect_to login_path
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/', notice: "Logged Out!"
  end
end
