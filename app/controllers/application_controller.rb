class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  

  private 
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def role_user
    @role_user ||= User.find(session[:user_id]).role if session[:user_id]
  end
  helper_method :role_user

  def authorize
    redirect_to login_url, alert: "Not authorized" if current_user.nil?
  end
  helper_method :authorize

  def paid?
    redirect_to subscription_url( id: current_user.subscription.id), alert: "Please subscribe to view" if current_user.subscription.paid.blank?
  end
  helper_method :paid?
end
