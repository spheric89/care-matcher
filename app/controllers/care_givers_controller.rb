class CareGiversController < ApplicationController

  before_filter :authorize, only: [:update, :edit]

  def show
    @caregiver = CareGiver.find(params[:id])
  end

  def edit
    @caregiver = CareGiver.find(params[:id])
  end

  def update
    @caregiver = CareGiver.find(params[:id])
    if @caregiver.update(care_giver_params)
      flash[:success] = "Profile Updated"
      redirect_to care_giver_path
    else
      render 'edit'
    end
  end

  def index
    @caregivers = CareGiver.where(hidden: false).text_search(params[:query], params[:hourly_rate])
  end
private 

  def care_giver_params
    params.require(:care_giver).permit(:first_name, :last_name, :hourly_rate, :description, :photo)
  end
end
