class UsersController < ApplicationController

before_filter :authorize, only: [:show, :index]

def show
  @user = current_user
  @activities = PublicActivity::Activity.all
end

def index
end

def new
  @user = User.new
end

def create
  @user = User.new(user_params.except(:role))
  role = user_params[:role]
  if (@user.role = @user.choose_role(role)) && @user.save
    @user.new_subscription
    # UserMailer.signup_confirmation(@user).deliver
    redirect_to '/', notice: "Thank you for signing up as a #{@user.role_type}!"
  else
    render "new"
  end
end

private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :postcode, :role)
  end

end
