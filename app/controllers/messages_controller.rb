class MessagesController < ApplicationController

  before_filter :get_mailbox, :get_box, :get_actor, :authorize
  
  def index
    redirect_to conversations_path(:box => @box)
  end

  #GET /messages/1
  # GET /messages/1.xml
  def show
    if @message = Message.find_by_id(params[:id]) and @conversation = @message.conversation
      if @conversation.is_participant?(@actor)
        redirect_to conversation_path(@conversation, :box => @box, :anchor => "message_" + @message.id.to_s)
        return
      end
    end
    redirect_to conversations_path(:box => @box)
  end

  # GET /messages/new
  # GET /messages/new.xml
  def new
    @message = Message.new
    if params.to_a[0][0] == "care_giver_id" || params.to_a[0][0] == "care_job_id"
      @recipient = string_manipulation.where(id: params.to_a[0][1]).first
    end
      return if @recipient.nil?
      #@recipient = nil if Actor.normalize(@recipient)==Actor.normalize(current_subject)
    #end
  end

  # GET /messages/1/edit
  def edit

  end

  # POST /messages
  # POST /messages.xml
  def create
    @recipients = 
      if params[:_recipients].present?
        @recipients = params[:_recipients].split(',').map{ |r| Actor.find(r) }
      else
        []
      end

    @receipt = @actor.send_message(@recipients, params[:body], params[:subject])
    if (@receipt.errors.blank?)
      @conversation = @receipt.conversation
      flash[:success]= t('mailboxer.sent')
      redirect_to conversation_path(@conversation, :box => :sentbox)
    else
      render :action => :new
    end
  end

  # PUT /messages/1
  # PUT /messages/1.xml
  def update

  end

  # DELETE /messages/1
  # DELETE /messages/1.xml
  def destroy

  end

  def trash
    receipt = Receipt.where(id: params[:id]).first
    conv_id = receipt.conversation
    role_user.trash(receipt) 
    redirect_to conversation_path(id: conv_id[:id])
  end

  def untrash
    receipt = Receipt.where(id: params[:id]).first
    conv_id = receipt.conversation
    role_user.untrash(receipt)
    redirect_to conversation_path(id: conv_id[:id])
  end

  private

  def get_mailbox
    @mailbox = role_user.mailbox
  end

  def get_actor
    @actor = role_user
  end

  def get_box
    if params[:box].blank? or !["inbox","sentbox","trash"].include?params[:box]
      @box = "inbox"
      return
    end
    @box = params[:box]
  end

  def string_manipulation
    new_array = params.to_a[0][0].split('_')[0..1].each do |string|
      string.capitalize!
    end
    final_array = new_array[0] + new_array[1]
    eval(final_array)
  end

end
