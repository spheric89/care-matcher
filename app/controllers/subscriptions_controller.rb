class SubscriptionsController < ApplicationController

  before_filter :authorize

  def show
    @subscription = current_user.subscription
  end
  def new

  end

  def create
    @subscription = current_user.subscription
    if current_user.role_type == "CareFinder"
      customer = Stripe::Customer.create(
        :email => current_user.email,
        :card  => params[:stripetoken],
        :plan  => 'carerecipient'
      )
    elsif current_user.role_type == "CareGiver"
      customer = Stripe::Customer.create(
        :email => current_user.email,
        :card  => params[:stripetoken],
        :plan  => 'carer'
      )
    end

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to subscriptions_path
  else
    @subscription.stripe_customer_token = customer.id
    @subscription.paid = true 
    @subscription.save!
  end

end
