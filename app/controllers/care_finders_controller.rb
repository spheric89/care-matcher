class CareFindersController < ApplicationController

  before_filter :authorize

  def show
    @carefinder = CareFinder.find(params[:id])
    @user = User.where(role_id: params[:id]).first
  end
  
  def edit
    @carefinder = CareFinder.find(params[:id])
  end 

  def update
    @carefinder = CareFinder.find(params[:id])
    if @carefinder.update(care_finder_params)
      flash[:success] = "Profile Updated"
      redirect_to '/'
    else
      flash[:failure] = "Profile update failed"
      render 'edit'
    end
  end

  def destroy

  end

  def jobs
    @carejobs = CareJob.where(care_finder_id: params[:id], hidden: false)
  end

private

  def care_finder_params
    params.require(:care_finder).permit(:last_name, :first_name, :postcode)
  end
end
