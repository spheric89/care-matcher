class ContactsController < ApplicationController
  before_filter :is_owner?, only: :accept
  before_filter :authorize
  before_filter :paid?

  def new
    @contact = current_user.role.contacts.new
    @contact[:to] = @contact.to_name(params.first)
    @contact[@contact.find_id(params.first[0])] = params.first[1]
  end

  def create
    @contact = current_user.role.contacts.new(contact_params)
    @contact[:from] = @contact.from_name(current_user.role_type, current_user.role_id)
    @contact[:owner_role] = current_user.role_type

    if @contact.save
      flash[:success] = "Contact Request Sent!"
      redirect_to contacts_path
    else
      render "new"
      Rails.logger.info(@contact.errors.inspect) 
    end
  end

  def index
    @contacts = role_user.contacts.where.not(state: 'declined')
  end

  def decline
    @contact = role_user.contacts.where(id: params[:id]).first
    if @contact.decline
      role_user.reply_to_conversation(@contact.mailbox.conversations.first, params[:contact][:message])
      flash[:success] = "Contact Request Declined!"
      redirect_to contact_path
    else
      flash[:failure] = "Decline failed!"
      redirect_to contact_path
    end
  end

  def accept
    @contact = role_user.contacts.where(id: params[:id]).first
    if @contact.accept
      role_user.send_message(@contact.to_msg(current_user.role_type), params[:contact][:message], "Contact Accepted")
      flash[:success] = "Contact Request Accepted!"
      redirect_to contact_path
    else
      flash[:failure] = "Accept failed!"
      redirect_to contact_path
    end
  end
  
  def show
    @contact = Contact.where(id: params[:id]).first
    @owner = is_owner_view?
  end



private
  
  def contact_params
    params.require(:contact).permit(:to, :message, :care_job_id, :care_giver_id)
  end

  def is_owner?
    if name = role_user.contacts.where(id: params[:id]).first
      if name.owner_role == role_user.class.name
      flash[:error] = "Unauthorised Action"
      redirect_to contact_path(params[:id])
      end
    else
    end
  end

  def is_owner_view?
    if name = role_user.contacts.where(id: params[:id]).first
      if name.owner_role == role_user.class.name
        true
      end
    else
      false
    end
  end




end
