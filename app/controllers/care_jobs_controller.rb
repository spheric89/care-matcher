class CareJobsController < ApplicationController

  before_filter :authorize, only: [:new, :destroy, :edit, :update]

  def index
    @carejobs = CareJob.where(hidden: false).text_search(params[:query])
  end

  def show
    @carejob = CareJob.where(id: params[:id]).first
  end

  def new
    @carejob = current_user.role.care_jobs.new
  end

  def create
    @carejob = current_user.role.care_jobs.new(care_job_params)
    @carejob.save
    @carejob.create_activity(:create, owner: current_user)
    redirect_to care_finder_jobs_path(current_user.role.id)
  end
  
  def destroy
    if CareJob.delete_care_job(params[:id])
      flash[:success] = "Care Job Deleted!"
      redirect_to care_finder_jobs_path(current_user.role.id)
    else
      render "index"
    end
  end

  def edit
    @carejob = CareJob.find(params[:id])
  end
  
  def update
    if CareJob.update(params[:id], care_job_params)
      flash[:success] = "Care Job Edited!"
      redirect_to care_finder_jobs_path(current_user.role.id)
    else
      render "edit"
    end
  end

private

  def care_job_params
    params.require(:care_job).permit(:name, :rate, :description, :photo)
  end


end
