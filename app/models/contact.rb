class Contact < ActiveRecord::Base
  belongs_to :care_giver
  belongs_to :care_job
  validates :care_job_id, presence: true
  
  acts_as_messageable
  state_machine :state, initial: :requested do

    event :decline do 
      transition requested: :declined,
                 accepted: :declined
    end

    event :accept do 
      transition requested: :accepted,
                 declined: :accepted
    end

  end


  def get_messages
    unless (conversation ||= self.mailbox.conversations.where(subject: "#{id}")).empty?
      conversation.first.receipts_for(self)
    end
  end

  def mailboxer_email(object)

  end

  def find_id(param_id)
    if param_id == "care_job_id"
      :care_job_id
    elsif param_id == "care_giver_id"
      :care_giver_id
    end
  end

  def to_name(role)
    if role[0] == "care_giver_id"
      caregiver_name(role[1])
    elsif role[0] == "care_job_id"
      carefinder_name(role[1])
    end
  end

  def from_name(role_type, role_id)
    if role_type == "CareGiver"
      caregiver_name(role_id)
    elsif role_type == "CareFinder"
      carefinder_name(role_id)
    end
  end

  def to_msg(role_type)
    if owner_role == role_type
      to_msg_as_owner
    else
      to_msg_not_as_owner
    end
  end

  def to_msg_as_owner
    if owner_role == "CareFinder"
      CareGiver.where(id: care_giver_id).first
    elsif owner_role == "CareGiver"
      self.care_job.care_finder
    end
  end

  def to_msg_not_as_owner
    if owner_role == "CareGiver"
      CareGiver.where(id: care_giver_id).first
    elsif owner_role == "CareFinder"
      self.care_job.care_finder
    end
  end

private
  def caregiver_name(role)
    name = CareGiver.where(id: role).first
    fullname(name)
  end

  def carefinder_name(role)
    name = CareJob.where(id: role).first.care_finder
    fullname(name)
  end

  def fullname(name)
    "#{name.first_name} #{name.last_name}"
  end
end
