class Subscription < ActiveRecord::Base
  belongs_to :user

  def self.setup_plan(user)
    subscription = Subscription.new(user_id: user.id)

    if user.role_type == "CareFinder"
      subscription[:name] = "Looking for Care Plan"
      subscription[:price] = 60 
    elsif
      subscription[:name] = "Providing Care Plan"
      subscription[:price] = 30 
    end

    subscription.save
  end

end
