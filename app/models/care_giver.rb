class CareGiver < ActiveRecord::Base
  acts_as_messageable
  
  has_one :user, as: :role
  has_many :contacts
  has_many :qualifications
  has_many :experiences
  has_many :availabilities
  has_many :interests
  has_many :hobbies
  has_many :pref_locations
  

  has_attached_file :photo, styles: { medium: "300x300", thumb: "100x100" }, default_url: "CFA.jpg"

  include PgSearch
  pg_search_scope :search, (lambda do |query, *args|
    return { against: :args, query: :query }
  end)

  def self.text_search(query, hourly_rate)
   if query.present?
     search(query, hourly_rate)
   else
    scoped
   end
  end

  def mailboxer_email(object)
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
