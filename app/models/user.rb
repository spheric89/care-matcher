class User < ActiveRecord::Base
  has_secure_password
  has_one :subscription
  validates :email, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validates :postcode, presence: true
  validates :role_type, inclusion: { in: %w(CareGiver CareFinder) }
  belongs_to :role, polymorphic: true
  validates :password, presence: true, length: { within: 8..40 } 

  def choose_role(role)
    if care_giver?(role)
      role = CareGiver.create
    elsif care_finder?(role)
      role = CareFinder.create
    end
  end


  def new_subscription
    Subscription.setup_plan(self)
  end

private

  def care_giver?(role)
    role == "caregiver"
  end

  def care_finder?(role)
    role == "carefinder"
  end
end
