class CareFinder < ActiveRecord::Base
  has_one :user, as: :role
  has_many :contacts, through: :care_jobs
  has_many :care_jobs

  acts_as_messageable
  def mailboxer_email(object)

  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
