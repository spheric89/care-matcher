class CareJob < ActiveRecord::Base
  include PublicActivity::Common
   
  belongs_to :care_finder
  has_many :contacts
  has_many :availabilities
  has_many :care_needs
  has_many :qualifications
  has_many :experiences
  has_many :interests
  has_many :hobbies
  has_many :pref_locations
  
  has_attached_file :photo, styles: { medium: "300x300", thumb: "100x100" }, default_url: "/images/CFA.jpg"

  def self.delete_care_job(id)
    CareJob.where(id: id).first.update_attributes(hidden: true)
  end

  include PgSearch
  pg_search_scope :search, against: [:name, :description]

  def self.text_search(query)
   if query.present?
     search(query)
   else
    scoped
   end
  end
end
